﻿using System;
using System.Web.Security;
using LightLDAP.Data;

namespace Sitecore.SecurityExtensions.LDS.Data
{
	public class SitecoreLdsMembershipUser : ActiveDirectoryMembershipUser
	{
		// Fields
		public bool CommentModified { get; set; }
		public bool EmailModified { get; set; }
		public bool IsApprovedModified { get; set; }
		private DateTime _lastActivityDate;
		private DateTime _lastLoginDate;

		// Methods
		public SitecoreLdsMembershipUser(MembershipUser user, bool valuesAreUpdated)
			: this(user.ProviderName, user.UserName, user.ProviderUserKey, user.Email, user.PasswordQuestion, user.Comment, user.IsApproved, user.IsLockedOut, user.CreationDate, DateTime.MinValue, DateTime.MinValue, user.LastPasswordChangedDate, user.LastLockoutDate, valuesAreUpdated)
		{
		}

		public SitecoreLdsMembershipUser(string providerName, string name, object providerUserKey, string email, string passwordQuestion, string comment, bool isApproved, bool isLockedOut, DateTime creationDate, DateTime lastLoginDate, DateTime lastActivityDate, DateTime lastPasswordChangedDate, DateTime lastLockoutDate, bool valuesAreUpdated)
			: base(providerName, name, providerUserKey, email, passwordQuestion, comment ?? "Please enter a comment here", isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockoutDate)
		{
			EmailModified = true;
			CommentModified = true;
			IsApprovedModified = true;
			EmailModified = valuesAreUpdated;
			CommentModified = valuesAreUpdated;
			IsApprovedModified = valuesAreUpdated;
		}

		public static implicit operator SitecoreLdsMembershipUser(UserDefinition user)
		{
			return new SitecoreLdsMembershipUser(user.ProviderName, user.UserName, user.ProviderUserKey, user.Email, user.PasswordQuestion, user.Comment, user.IsApproved, user.IsLockedOut, user.CreationDate, user.LastLoginDate, user.LastActivityDate, user.LastPasswordChangedDate, user.LastLockoutDate, false);
		}

		// Properties
		public override string Comment
		{
			get
			{
				return base.Comment;
			}
			set
			{
				base.Comment = value;
				CommentModified = true;
			}
		}

		public override string Email
		{
			get
			{
				return base.Email;
			}
			set
			{
				base.Email = value;
				EmailModified = true;
			}
		}

		public override bool IsApproved
		{
			get
			{
				return base.IsApproved;
			}
			set
			{
				base.IsApproved = value;
				IsApprovedModified = true;
			}
		}

		public override DateTime LastActivityDate
		{
			get
			{
				return DateTime.MinValue;
			}
			set
			{
				_lastActivityDate = value;
			}
		}

		public override DateTime LastLoginDate
		{
			get
			{
				return DateTime.MinValue;
			}
			set
			{
				_lastLoginDate = value;
			}
		}
	}
}
