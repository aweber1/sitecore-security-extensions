﻿using System;
using System.Globalization;
using Sitecore.Diagnostics;
using SR = Sitecore.SecurityExtensions.LDS.Resources.SR;

namespace Sitecore.SecurityExtensions.LDS.Utility
{
	public class CheckUtil
	{
		// Methods
		internal static void CheckParameter(ref string param, bool checkForNull, bool checkIfEmpty, int maxSize,
											string paramName)
		{
			if (checkForNull)
			{
				Assert.ArgumentNotNull(param, paramName);
			}
			param = param.Trim();
			if (checkIfEmpty && (param.Length < 1))
			{
				throw new ArgumentException(SR.GetString(SR.PARAM_CANT_BE_EMPTY, new object[] { paramName }), paramName);
			}
			if ((maxSize > 0) && (param.Length > maxSize))
			{
				throw new ArgumentException(
					SR.GetString(SR.PARAM_IS_TOO_LONG, new object[] { paramName, maxSize.ToString(CultureInfo.InvariantCulture) }),
					paramName);
			}
		}

		internal static void StopIfNotInitialized(bool value)
		{
			if (!value)
			{
				throw new InvalidOperationException(SR.GetString(SR.NOT_INITIALIZED));
			}
		}
	}


}
