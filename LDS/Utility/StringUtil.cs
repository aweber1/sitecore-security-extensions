﻿using System;
using System.Globalization;
using System.Text;
using Sitecore.StringExtensions;

namespace Sitecore.SecurityExtensions.LDS.Utility
{
	public class StringUtil
	{
		// Methods
		public static string BuildOctetString(byte[] bytes)
		{
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < bytes.Length; i++)
			{
				builder.AppendFormat(@"\{0}", bytes[i].ToString("X2"));
			}
			return builder.ToString();
		}

		public static bool GetBool(string value, bool defaultValue)
		{
			bool flag;
			if (string.IsNullOrEmpty(value))
			{
				return defaultValue;
			}
			if (!bool.TryParse(value, out flag))
			{
				throw new ArgumentException("Value must be boolean", value);
			}
			return flag;
		}

		public static int GetIntValue(string value, int defaultValue, bool zeroAllowed, int maxValueAllowed)
		{
			int num;
			if (string.IsNullOrEmpty(value))
			{
				return defaultValue;
			}
			if (!int.TryParse(value, out num))
			{
				if (zeroAllowed)
				{
					throw new ArgumentException("Value must be non negative integer", value);
				}
				throw new ArgumentException("Value must be positive integer", value);
			}
			if (zeroAllowed && (num < 0))
			{
				throw new ArgumentException("Value must be non negative integer", value);
			}
			if (!zeroAllowed && (num <= 0))
			{
				throw new ArgumentException("Value must be positive integer", value);
			}
			if ((maxValueAllowed > 0) && (num > maxValueAllowed))
			{
				throw new ArgumentException("Value too big: {0}".FormatWith(new object[] { maxValueAllowed.ToString(CultureInfo.InvariantCulture) }), value);
			}
			return num;
		}
	}


}
