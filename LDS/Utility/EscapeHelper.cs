﻿using System.Collections.Generic;
using System.Text;

namespace Sitecore.SecurityExtensions.LDS.Utility
{
	public class EscapeHelper
	{
		// Methods
		public static string EscapeCharacters(string source)
		{
			return EscapeCharacters(source, true);
		}

		public static string EscapeCharacters(string source, bool escapeAsterisk)
		{
			if (string.IsNullOrEmpty(source))
			{
				return source;
			}
			Dictionary<string, string> dictionary2 = new Dictionary<string, string>();
			dictionary2.Add(@"\\", @"\\5c");
			dictionary2.Add(@"\/", @"\2f");
			dictionary2.Add("(", @"\28");
			dictionary2.Add(")", @"\29");
			Dictionary<string, string> dictionary = dictionary2;
			if (escapeAsterisk)
			{
				dictionary.Add("*", @"\2a");
			}
			StringBuilder builder = new StringBuilder(source);
			foreach (string str in dictionary.Keys)
			{
				builder.Replace(str, dictionary[str]);
			}
			return builder.ToString();
		}
	}


}
