﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.DirectoryServices;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Security.Principal;
using System.Web.Security;
using LightLDAP;
using LightLDAP.ActiveDirectory;
using LightLDAP.Common;
using LightLDAP.Configurations;
using LightLDAP.Diagnostic;
using LightLDAP.Helpers;
using LightLDAP.Pipelines.InitializeAdEntry;
using LightLDAP.Utility;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using Sitecore.Security.Accounts;
using Sitecore.SecurityExtensions.LDS.Data;
using Sitecore.StringExtensions;
using DirectoryEntry = System.DirectoryServices.DirectoryEntry;
using SortDirection = System.DirectoryServices.SortDirection;
using SR = Sitecore.SecurityExtensions.LDS.Resources.SR;

namespace Sitecore.SecurityExtensions.LDS
{
	public class SitecoreLdsMembershipProvider : MembershipProvider
	{
		//Fields
		protected bool Initialized;
		protected MembershipProviderSettings Settings;
		protected DataSource.PartialDataSource Source;

		//Methods
		public SitecoreLdsMembershipProvider() : this(new AdMembershipProvider())
		{
		}

		public SitecoreLdsMembershipProvider(IAdMembershipProvider innerProvider)
		{
			InnerProvider = innerProvider;
			Settings = new MembershipProviderSettings();
		}

		public override bool ChangePassword(string username, string oldPassword, string newPassword)
		{
			if (!Initialized)
			{
				return false;
			}
			return (ValidateGlobalFilter(username) && InnerProvider.ChangePassword(username, oldPassword, newPassword));
		}

		public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
															 string newPasswordAnswer)
		{
			if (!Initialized)
			{
				return false;
			}
			return (ValidateGlobalFilter(username) &&
					InnerProvider.ChangePasswordQuestionAndAnswer(username, password, newPasswordQuestion, newPasswordAnswer));
		}

		public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion,
												  string passwordAnswer, bool isApproved, object providerUserKey,
												  out MembershipCreateStatus status)
		{
			ConditionLog.Debug(SR.GetString(SR.CREATE_USER_START), new object[] {username, password});
			if (!Initialized)
			{
				status = MembershipCreateStatus.ProviderError;
				return null;
			}
			if (RequiresQuestionAndAnswer)
			{
				passwordQuestion = "undefined";
				passwordAnswer = "undefined";
			}
			MembershipUser user = InnerProvider.CreateUser(username, password, email, passwordQuestion, passwordAnswer,
																isApproved, providerUserKey, out status);
			if (user != null)
			{
				SetCustomProperties(username);
				user = new SitecoreLdsMembershipUser(user, false);
				ConditionLog.Debug(SR.GetString(SR.CREATE_USER_DONE), new object[] {user.UserName});
				return user;
			}
			ConditionLog.Debug(SR.GetString(SR.CREATE_USER_FAILED));
			return null;
		}

		public override bool DeleteUser(string username, bool deleteAllRelatedData)
		{
			if (!Initialized)
			{
				return false;
			}
			Utility.CheckUtil.CheckParameter(ref username, true, true, Settings.MaxUsernameLength, "username");
			if (Settings.UserNameIsUpn && (username.IndexOf('\\') != -1))
			{
				throw new ArgumentException(SR.GetString(SR.GetString(SR.USER_CONTAINS_BACKSLASH), username));
			}
			IDirectoryEntry userEntryByUserName = GetUserEntryByUserName(username);
			if (userEntryByUserName == null)
			{
				return false;
			}
			try
			{
				IDirectoryEntry parent = userEntryByUserName.Parent;
				if (parent == null)
				{
					return false;
				}
				parent.Children.Remove(userEntryByUserName);
			}
			catch (COMException exception)
			{
				if (exception.ErrorCode != -2147016656)
				{
					throw;
				}
				return false;
			}
			finally
			{
				userEntryByUserName.Dispose();
			}
			Source.RemoveUser(username);
			return true;
		}

		private MembershipUserCollection FindUsers(string filter, string sortKey, int pageIndex, int pageSize,
												   out int totalRecords, bool narrowSearch)
		{
			MembershipUserCollection users2;
			MembershipUserCollection users = new MembershipUserCollection();
			using (IDirectorySearcher searcher = InnerProvider.GetAdSearcher())
			{
				searcher.SearchRoot = Settings.Root;
				SetProperties(searcher);
				string str = "(&(objectCategory=person)(objectClass=user)" + filter + ")";
				int num = (pageIndex + 1) * pageSize;
				int num2 = (num - pageSize) + 1;
				searcher.Filter = str;
				if (LightLDAP.Configurations.Settings.EnableSorting)
				{
					searcher.Sort = new SortOption(sortKey, SortDirection.Ascending);
				}
				searcher.PageSize = 0x200;
				using (ISearchResultCollection results = searcher.FindAll())
				{
					int num3 = 0;
					SizeLimitHolder currentValue = Switcher<SizeLimitHolder, SizeLimitHolder>.CurrentValue;
					int sizeLimit;
					if (currentValue != null)
					{
						sizeLimit = currentValue.SizeLimit;
					}
					else
					{
						sizeLimit = narrowSearch ? LightLDAP.Configurations.Settings.FindSizeLimit : LightLDAP.Configurations.Settings.SizeLimit;
					}
					try
					{
						IEnumerator enumerator = results.GetEnumerator();
						while (enumerator.MoveNext())
						{
							num3++;
							if ((num3 >= num2) && (num3 <= num))
							{
								MembershipUser membershipUserFromSearchResult = GetMembershipUserFromSearchResult((ISearchResult)enumerator.Current);
								users.Add(membershipUserFromSearchResult);
							}
							if ((num3 >= sizeLimit) && (sizeLimit > 0))
							{
								goto Label_00FB;
							}
						}
					}
					catch (Exception ex)
					{
						Log.Error("Error in FindUsers", ex, this);
					}
				Label_00FB:
					totalRecords = num3;
					users2 = users;
				}
			}
			return users2;
		}

		public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize,
																  out int totalRecords)
		{
			string str;
			ConditionLog.Debug(SR.GetString(SR.FIND_USERS_BY_EMAIL_START), new object[] { emailToMatch, pageIndex, pageSize });
			if (!Initialized)
			{
				totalRecords = 0;
				return new MembershipUserCollection();
			}
			ValidateSearchParameters(pageIndex, pageSize);
			if (emailToMatch != null)
			{
				EscapeBitHolder currentValue = Switcher<EscapeBitHolder, EscapeBitHolder>.CurrentValue;
				str =
					"({0}=*)({1}={2})".FormatWith(new object[]
						{
							Settings.AttributeMapUsername, Settings.AttributeMapEmail,
							((currentValue == null) || currentValue.EscapeBit)
								? Utility.EscapeHelper.EscapeCharacters(emailToMatch, false)
								: Settings.AttributeMapEmail
						});
			}
			else
			{
				str = "(" + Settings.AttributeMapUsername + "=*)(!(" + Settings.AttributeMapEmail + "=*))";
			}
			MembershipUserCollection users = FindUsers(str, LightLDAP.Configurations.Settings.DefaultSortKey, pageIndex, pageSize, out totalRecords,
															true);
			ConditionLog.Debug(SR.GetString(SR.FIND_USERS_BY_EMAIL_END), new object[] { users.Count });
			return GetFormattedUsers(users);
		}

		public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
																 out int totalRecords)
		{
			ConditionLog.Debug(SR.GetString(SR.FIND_USERS_BY_NAME_START), new object[] { usernameToMatch, pageIndex, pageSize });
			if (!Initialized)
			{
				totalRecords = 0;
				return new MembershipUserCollection();
			}
			ValidateSearchParameters(pageIndex, pageSize);
			EscapeBitHolder currentValue = Switcher<EscapeBitHolder, EscapeBitHolder>.CurrentValue;
			string filter =
				"({0}={1})".FormatWith(new object[]
					{
						Settings.AttributeMapUsername,
						((currentValue == null) || currentValue.EscapeBit)
							? Utility.EscapeHelper.EscapeCharacters(usernameToMatch, false)
							: usernameToMatch
					});
			MembershipUserCollection users = FindUsers(filter, LightLDAP.Configurations.Settings.DefaultSortKey, pageIndex, pageSize,
															out totalRecords, usernameToMatch != "*");
			ConditionLog.Debug(SR.GetString(SR.FIND_USERS_BY_NAME_END), new object[] { users.Count });
			return GetFormattedUsers(users);
		}

		public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
		{
			ConditionLog.Debug(SR.GetString(SR.GET_ALL_USERS_START), new object[] { pageIndex, pageSize });
			if (!Initialized)
			{
				totalRecords = 0;
				return new MembershipUserCollection();
			}
			MembershipUserCollection users = FindUsersByName("*", pageIndex, pageSize, out totalRecords);
			ConditionLog.Debug(SR.GetString(SR.GET_ALL_USERS_END), new object[] { users.Count });
			return GetFormattedUsers(users);
		}

		private MembershipUserCollection GetFormattedUsers(MembershipUserCollection users)
		{
			MembershipUserCollection users2 = new MembershipUserCollection();
			foreach (MembershipUser user in users)
			{
				SitecoreLdsMembershipUser user2 = new SitecoreLdsMembershipUser(user.ProviderName, user.UserName, user.ProviderUserKey,
																			  user.Email, user.PasswordQuestion, user.Comment,
																			  user.IsApproved, user.IsLockedOut, user.CreationDate,
																			  DateTime.MinValue, DateTime.MinValue,
																			  user.LastPasswordChangedDate, user.LastLockoutDate,
																			  false);
				users2.Add(user2);
				if (Source.NotificationProvider == null)
				{
					Source.UpdateUser(user2.UserName, user2);
				}
			}
			return users2;
		}

		private MembershipUser GetMembershipUserFromSearchResult(ISearchResult res)
		{
			bool flag2;
			string name = DataHelper.GetString(res, Settings.AttributeMapUsername);
			byte[] propertyValue = (byte[])DataHelper.GetPropertyValue(res, "objectSid");
			SecurityIdentifier providerUserKey = new SecurityIdentifier(propertyValue, 0);
			string email = DataHelper.GetString(res, Settings.AttributeMapEmail, false);
			string comment = DataHelper.GetString(res, ObjectAttribute.Comment, false);
			string passwordQuestion = null;
			if (Settings.AttributeMapPasswordQuestion != null)
			{
				passwordQuestion = DataHelper.GetString(res, Settings.AttributeMapPasswordQuestion, false);
			}
			bool isLockedOut = false;
			if (Settings.DirectoryType == 0)
			{
				flag2 = (DataHelper.GetInt(res, ObjectAttribute.UserAccountControl) & 2) == 0;
				if (res.Properties.Contains(ObjectAttribute.UserAccountControlComputed))
				{
					if ((DataHelper.GetInt(res, ObjectAttribute.UserAccountControlComputed) & 0x10) != 0)
					{
						isLockedOut = true;
					}
				}
				else if (res.Properties.Contains(ObjectAttribute.LockoutTime))
				{
					DateTime dateTimeUtc = DataHelper.GetDateTimeUtc(res, ObjectAttribute.LockoutTime);
					isLockedOut = DateTime.UtcNow.Subtract(dateTimeUtc) <= Settings.AdLockoutDuration;
				}
			}
			else
			{
				flag2 = true;
				if (res.Properties.Contains(ObjectAttribute.MsDSUserAccountDisabled))
				{
					flag2 = !DataHelper.GetBool(res, ObjectAttribute.MsDSUserAccountDisabled);
				}
				
				foreach (var key in res.Properties.PropertyNames)
				{
					var propVals = res.Properties[key.ToString()].Cast<object>().Aggregate(string.Empty, (current, propVal) => current + (propVal + ","));
					Log.Debug("property key: " + key + " | value: " + propVals.TrimEnd(','));
				}
				/*
					Original code:
					if ((DataHelper.GetInt(res, ObjectAttribute.UserAccountControlComputed) & 0x10) != 0)
				*/
				if ((DataHelper.GetInt(res, "msDS-User-Account-Control-Computed") & 0x10) != 0)
				{
					isLockedOut = true;
				}
			}
			DateTime defaultLastLockoutDate = Settings.DefaultLastLockoutDate;
			if (isLockedOut)
			{
				defaultLastLockoutDate = DataHelper.GetDateTimeLong(res, ObjectAttribute.LockoutTime);
			}
			DateTime time3 = DataHelper.GetDateTimeLong(res, ObjectAttribute.LockoutTime, false);
			DateTime dateTimeLong = DataHelper.GetDateTimeLong(res, Settings.AttributeMapFailedPasswordAnswerLockoutTime);
			if ((EnablePasswordReset && !string.IsNullOrEmpty(Settings.AttributeMapFailedPasswordAnswerLockoutTime)) &&
				res.Properties.Contains(Settings.AttributeMapFailedPasswordAnswerLockoutTime))
			{
				DateTime time5 = DataHelper.GetDateTimeUtc(res, Settings.AttributeMapFailedPasswordAnswerLockoutTime);
				if (DateTime.UtcNow.Subtract(time5) <= new TimeSpan(0, Settings.PasswordAnswerAttemptLockoutDuration, 0))
				{
					if (isLockedOut)
					{
						if (DateTime.Compare(time5, time3) > 0)
						{
							defaultLastLockoutDate = dateTimeLong;
						}
					}
					else
					{
						isLockedOut = true;
						defaultLastLockoutDate = dateTimeLong;
					}
				}
			}
			return new ActiveDirectoryMembershipUser(Name, name, providerUserKey, email, passwordQuestion, comment, flag2,
													 isLockedOut,
													 DataHelper.GetDateTime(res, ObjectAttribute.WhenCreated).ToLocalTime(),
													 DateTime.MinValue, DateTime.MinValue,
													 DataHelper.GetDateTimeLong(res, ObjectAttribute.PwdLastSet),
													 defaultLastLockoutDate);
		}

		public override int GetNumberOfUsersOnline()
		{
			if (Initialized)
			{
				return InnerProvider.GetNumberOfUsersOnline();
			}
			return 0;
		}

		public override string GetPassword(string username, string answer)
		{
			if (Initialized)
			{
				return InnerProvider.GetPassword(username, answer);
			}
			return null;
		}

		public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
		{
			ConditionLog.Debug(SR.GetString(SR.GET_USER_PK_START), new[] {providerUserKey, userIsOnline});
			if (Initialized)
			{
				MembershipUser user = InnerProvider.GetUser(providerUserKey, userIsOnline);
				if (user != null)
				{
					ConditionLog.Debug(SR.GetString(SR.GET_USER_PK_FOUND), new object[] {user.UserName});
					user = new SitecoreLdsMembershipUser(user, false);
					Source.UpdateUser(user.UserName, user);
					return user;
				}
				ConditionLog.Debug(SR.GetString(SR.GET_USER_PK_NOT_FOUND));
			}
			return null;
		}

		public override MembershipUser GetUser(string username, bool userIsOnline)
		{
			ConditionLog.Debug(SR.GetString(SR.GET_USER_START), new object[] {username, userIsOnline});
			if (Initialized)
			{
				if (string.IsNullOrEmpty(username))
				{
					return null;
				}
				MembershipUser user = Source.GetUser(username);
				if (user != null)
				{
					return user;
				}
				if (ValidateGlobalFilter(username))
				{
					user = InnerProvider.GetUser(username, userIsOnline);
				}
				if (user != null)
				{
					ConditionLog.Debug(SR.GetString(SR.GET_USER_FOUND), new object[] {user.UserName});
					user = new SitecoreLdsMembershipUser(user, false);
					Source.UpdateUser(username, user);
					return user;
				}
				ConditionLog.Debug(SR.GetString(SR.GET_USER_NOT_FOUND));
			}
			return null;
		}

		private IDirectoryEntry GetUserEntry(string filter)
		{
			using (IDirectorySearcher searcher = InnerProvider.GetAdSearcher())
			{
				return searcher.FindOneEntry(Settings.Root, filter, LightLDAP.Configurations.Settings.DeleteUserScope);
			}
		}

		private IDirectoryEntry GetUserEntryByEmail(string email)
		{
			string filter = "(&(objectCategory=person)(objectClass=user)(" + Settings.AttributeMapUsername + "=*)(" +
							Settings.AttributeMapEmail + "=" + Utility.EscapeHelper.EscapeCharacters(email, false) + "))";
			return GetUserEntry(filter);
		}

		private IDirectoryEntry GetUserEntryByUserName(string username)
		{
			string filter =
				"(&(objectCategory=person)(objectClass=user)({0}={1}))".FormatWith(new object[]
					{Settings.AttributeMapUsername, username});
			return GetUserEntry(filter);
		}

		public override string GetUserNameByEmail(string email)
		{
			if (Initialized && ValidateGlobalFilterWithEmail(email))
			{
				return InnerProvider.GetUserNameByEmail(email);
			}
			return null;
		}

		public override void Initialize(string name, NameValueCollection config)
		{
			try
			{
				ApplicationName = config["applicationName"];
				Settings.Initialize(config);
				config.Remove("serverPageTimeLimit");
				config.Remove("useNotification");
				try
				{
					var isInnerFailed = false;
					try
					{
						InnerProvider.Initialize(name, config);
					}
					catch (Exception ex)
					{
						isInnerFailed = true;
						Log.Error("LDS innerprovider initialize failed", ex, this);
					}
					try
					{
						Settings.InitializeHidden(InnerProvider, isInnerFailed);
						Source = DataSource.DataStorageContainer.GetPartialSource(Settings.AdsPath, Settings.UserName,
																			Settings.Password, Settings.AttributeMapUsername,
																			Settings.UseNotification);
						Initialized = true;
					}
					catch (Exception ex)
					{
						Log.Error("Error in LDS membership provider initialize", ex, this);
					}
					base.Initialize(name, config);
				}
				catch (Exception exception)
				{
					Log.Error("The LDS membership provider couldn't be initialized: " + exception.Message, exception, this);
				}
			}
			catch (Exception exception2)
			{
				Log.Error("LDS:", exception2, this);
				throw;
			}
		}

		private bool IsEmailUnique(string username, string email, bool existing)
		{
			string str = existing ? username : "*";
			string filter =
				"(&(objectCategory=person)(objectClass=user)({0}={1})({2}={3}))".FormatWith(new object[]
					{Settings.AttributeMapUsername, str, Settings.AttributeMapEmail, Utility.EscapeHelper.EscapeCharacters(email)});
			using (IDirectorySearcher searcher = InnerProvider.GetAdSearcher())
			{
				return (searcher.FindOne(Settings.Root, filter) == null);
			}
		}

		public override string ResetPassword(string username, string answer)
		{
			Assert.ArgumentNotNullOrEmpty(username, "username");
			if (Initialized && ValidateGlobalFilter(username))
			{
				return InnerProvider.ResetPassword(username, answer);
			}
			return null;
		}

		private void SetCustomProperties(string username)
		{
			using (IDirectorySearcher searcher = InnerProvider.GetAdSearcher())
			{
				if (searcher is DefaultSearcher)
				{
					searcher.Filter =
						"(&(objectCategory=person)(objectClass=user)({0}={1}))".FormatWith(new object[]
							{Settings.AttributeMapUsername, username});
					ISearchResult result = searcher.FindOne();
					if ((result != null) && (result.GetDirectoryEntry().InnerObject is DirectoryEntry))
					{
						InitializeAdEntryArgs args = new InitializeAdEntryArgs((DirectoryEntry) result.GetDirectoryEntry().InnerObject,
																			   (DirectoryEntry) Settings.Root.InnerObject);
						CorePipeline.Run("initializeAdUserEntry", args);
					}
				}
			}
		}

		private void SetProperties(IDirectorySearcher searcher)
		{
			Assert.ArgumentNotNull(searcher, "searcher");
			searcher.PropertiesToLoad.Add(Settings.AttributeMapUsername);
			searcher.PropertiesToLoad.Add("objectSid");
			searcher.PropertiesToLoad.Add(Settings.AttributeMapEmail);
			searcher.PropertiesToLoad.Add("comment");
			searcher.PropertiesToLoad.Add("whenCreated");
			searcher.PropertiesToLoad.Add("pwdLastSet");
			searcher.PropertiesToLoad.Add("msDS-User-Account-Control-Computed");
			searcher.PropertiesToLoad.Add("lockoutTime");
			searcher.PropertiesToLoad.Add(Settings.DirectoryType == 0 ? "userAccountControl" : "msDS-UserAccountDisabled");
			if (EnablePasswordReset)
			{
				searcher.PropertiesToLoad.Add(Settings.AttributeMapFailedPasswordAnswerCount);
				searcher.PropertiesToLoad.Add(Settings.AttributeMapFailedPasswordAnswerTime);
				searcher.PropertiesToLoad.Add(Settings.AttributeMapFailedPasswordAnswerLockoutTime);
			}
			if (Settings.ClientSearchTimeout != -1)
			{
				searcher.ClientTimeout = new TimeSpan(0, Settings.ClientSearchTimeout, 0);
			}
			if (Settings.ServerSearchTimeout != -1)
			{
				searcher.ServerTimeLimit = new TimeSpan(0, Settings.ServerSearchTimeout, 0);
			}
			if (Settings.ServerPageTimeLimit != -1)
			{
				searcher.ServerPageTimeLimit = new TimeSpan(0, 0, Settings.ServerPageTimeLimit);
			}
			if (Settings.AttributeMapPasswordQuestion != null)
			{
				searcher.PropertiesToLoad.Add(Settings.AttributeMapPasswordQuestion);
			}
		}

		public override bool UnlockUser(string username)
		{
			if (Initialized && ValidateGlobalFilter(username))
			{
				bool flag = InnerProvider.UnlockUser(username);
				Source.RemoveUser(username);
				return flag;
			}
			return false;
		}

		private bool UpdateUser(SitecoreLdsMembershipUser adUser)
		{
			string email = adUser.Email;
			if (adUser.EmailModified)
			{
				Utility.CheckUtil.CheckParameter(ref email, RequiresUniqueEmail, true, Settings.MaxEmailLength, "Email");
			}
			string comment = adUser.Comment;
			if (adUser.CommentModified)
			{
				Utility.CheckUtil.CheckParameter(ref comment, false, false, Settings.MaxCommentLength, "Comment");
			}
			if ((adUser.EmailModified || adUser.CommentModified) || adUser.IsApprovedModified)
			{
				using (var searcher = InnerProvider.GetAdSearcher())
				{
					string filter =
						"(&(objectCategory=person)(objectClass=user)({0}={1}))".FormatWith(new object[] { Settings.AttributeMapUsername, Utility.EscapeHelper.EscapeCharacters(adUser.UserName) });
					using (var entry = searcher.FindOneEntry(Settings.Root, filter))
					{
						if (entry == null)
						{
							throw new ProviderException(SR.GetString("Membership_UserNotFound"));
						}

						if (adUser.EmailModified)
						{
							if (email == null)
							{
								if (entry.Properties.Contains(Settings.AttributeMapEmail))
								{
									entry.Properties[Settings.AttributeMapEmail].Clear();
								}
							}
							else
							{
								if (RequiresUniqueEmail && !IsEmailUnique(adUser.UserName, email, true))
								{
									throw new ProviderException(SR.GetString(SR.DUPLICATED_EMAIL));
								}
								entry.Properties[Settings.AttributeMapEmail].Value = email;
							}
						}
						if (adUser.CommentModified)
						{
							if (comment == null)
							{
								if (entry.Properties.Contains(ObjectAttribute.Comment))
								{
									entry.Properties[ObjectAttribute.Comment].Clear();
								}
							}
							else
							{
								entry.Properties[ObjectAttribute.Comment].Value = comment;
							}
						}
						if (adUser.IsApprovedModified)
						{
							if (Settings.DirectoryType == 0)
							{
								int @int = DataHelper.GetInt(entry, ObjectAttribute.UserAccountControl);
								if (adUser.IsApproved)
								{
									@int &= -3;
								}
								else
								{
									@int |= 2;
								}
								entry.Properties[ObjectAttribute.UserAccountControl].Value = @int;
							}
							else
							{
								entry.Properties[ObjectAttribute.MsDSUserAccountDisabled].Value = !adUser.IsApproved;
							}
						}
						entry.CommitChanges();
						adUser.EmailModified = false;
						adUser.CommentModified = false;
						adUser.IsApprovedModified = false;
						return true;
					}
				}
			}
			return false;
		}

		public override void UpdateUser(MembershipUser user)
		{
			ConditionLog.Debug(SR.GetString(SR.UPDATE_USER_START), new object[] {user.UserName});
			if (Initialized)
			{
				SitecoreLdsMembershipUser adUser = user as SitecoreLdsMembershipUser;
				if ((adUser == null) && (user is MembershipUserWrapper))
				{
					adUser = (user as MembershipUserWrapper).InnerUser as SitecoreLdsMembershipUser;
				}
				if (adUser != null)
				{
					if (string.IsNullOrEmpty(adUser.Comment))
					{
						ConditionLog.Debug(SR.GetString(SR.UPDATE_USER_COMMENT), new object[] {"Please enter a comment here"});
						adUser.Comment = "Please enter a comment here";
					}
					if (UpdateUser(adUser))
					{
						Source.RemoveUser(user.UserName);
					}
					ConditionLog.Debug(SR.GetString(SR.UPDATE_USER_DONE), new object[] {adUser.UserName});
				}
			}
		}

		private bool ValidateGlobalFilter(string userName)
		{
			return (string.IsNullOrEmpty(InnerProvider.GetAdSearcher().FilterProvider.CustomFilter) ||
					(!string.IsNullOrEmpty(userName) && (GetUserEntryByUserName(userName) != null)));
		}

		private bool ValidateGlobalFilterWithEmail(string email)
		{
			return (string.IsNullOrEmpty(InnerProvider.GetAdSearcher().FilterProvider.CustomFilter) ||
					(!string.IsNullOrEmpty(email) && (GetUserEntryByEmail(email) != null)));
		}

		private void ValidateSearchParameters(int pageIndex, int pageSize)
		{
			if (!Initialized)
			{
				throw new InvalidOperationException(SR.GetString(SR.NOT_INITIALIZED));
			}
			if (!Settings.EnableSearchMethods)
			{
				throw new NotSupportedException(SR.GetString(SR.SEARCH_NOT_SUPPORTED));
			}
			if (pageIndex < 0)
			{
				throw new ArgumentException(SR.GetString(SR.PAGE_INDEX_INVALID), "pageIndex");
			}
			if (pageSize < 1)
			{
				throw new ArgumentException(SR.PAGE_SIZE_INVALID, "pageSize");
			}
			long num = ((pageIndex*pageSize) + pageSize) - 1L;
			if (num > 0x7fffffffL)
			{
				throw new ArgumentException(SR.GetString(SR.PAGE_INVALID), "pageIndex and pageSize");
			}
		}

		[DirectoryServicesPermission(SecurityAction.LinkDemand, Unrestricted = true),
		 DirectoryServicesPermission(SecurityAction.InheritanceDemand, Unrestricted = true)]
		public override bool ValidateUser(string username, string password)
		{
			if (!Initialized)
			{
				return false;
			}
			if (!ValidateGlobalFilter(username))
			{
				return false;
			}
			
			if (InnerProvider.ValidateUser(username, password))
			{
				MembershipUser user = Source.GetUser(username) ?? GetUser(username, false);
				if (user == null || !user.IsApproved)
				{
					return false;
				}
				return user.IsApproved;
			}
			Source.RemoveUser(username);
			return false;
		}

		
		public override string ApplicationName { get; set; }

		public override bool EnablePasswordReset
		{
			get { return InnerProvider.EnablePasswordReset; }
		}

		public override bool EnablePasswordRetrieval
		{
			get { return InnerProvider.EnablePasswordRetrieval; }
		}

		public IAdMembershipProvider InnerProvider { get; private set; }

		public override int MaxInvalidPasswordAttempts
		{
			get { return InnerProvider.MaxInvalidPasswordAttempts; }
		}

		public override int MinRequiredNonAlphanumericCharacters
		{
			get { return InnerProvider.MinRequiredNonAlphanumericCharacters; }
		}

		public override int MinRequiredPasswordLength
		{
			get { return InnerProvider.MinRequiredPasswordLength; }
		}

		public override int PasswordAttemptWindow
		{
			get { return InnerProvider.PasswordAttemptWindow; }
		}

		public override MembershipPasswordFormat PasswordFormat
		{
			get { return InnerProvider.PasswordFormat; }
		}

		public override string PasswordStrengthRegularExpression
		{
			get { return InnerProvider.PasswordStrengthRegularExpression; }
		}

		public override bool RequiresQuestionAndAnswer
		{
			get { return InnerProvider.RequiresQuestionAndAnswer; }
		}

		public override bool RequiresUniqueEmail
		{
			get { return InnerProvider.RequiresUniqueEmail; }
		}
	}
}
