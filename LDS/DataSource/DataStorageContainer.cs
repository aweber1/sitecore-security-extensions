﻿using System.Collections;
using LightLDAP.Utility;
using Sitecore.Diagnostics;

namespace Sitecore.SecurityExtensions.LDS.DataSource
{
	public class DataStorageContainer
	{
		// Fields
		private static readonly object LockObject = new object();
		private static readonly Hashtable Storages = new Hashtable();

		// Methods
		public static PartialDataSource GetPartialSource(string path, string user, string password, string attributeMapName, bool userNotification)
		{
			Assert.ArgumentNotNullOrEmpty(path, "path");
			string[] strArray = new[] { LDAPPathUtil.GetServer(path).ToLower(), LDAPPathUtil.GetDC(path), (user ?? string.Empty).ToLower() };
			string key = string.Join("", strArray);
			if (Storages.Contains(key))
			{
				return (PartialDataSource)Storages[key];
			}
			lock (LockObject)
			{
				if (Storages.Contains(key))
				{
					return (PartialDataSource)Storages[key];
				}
				PartialDataSource source = new PartialDataSource(path, user, password, attributeMapName, userNotification);
				Storages.Add(key, source);
				return source;
			}
		}
	}
}
