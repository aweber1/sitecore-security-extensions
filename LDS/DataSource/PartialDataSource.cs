﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using LightLDAP.Caching;
using LightLDAP.Configurations;
using LightLDAP.Data;
using LightLDAP.Notification;
using LightLDAP.Notification.Events;
using LightLDAP.Utility;
using Sitecore.Diagnostics;
using Sitecore.SecurityExtensions.LDS.Data;

namespace Sitecore.SecurityExtensions.LDS.DataSource
{
	public class PartialDataSource
	{
		// Fields
		private readonly MemberOfCache _memberOfCache;
		private readonly MembersCache _membersCache;
		private readonly DirectoryNotificationProvider _notificationProvider;
		private readonly UserCache _userCache;

		// Methods
		internal PartialDataSource(string path, string user, string password, string attributeMapName, bool useNotification)
		{
			UseNotification = useNotification;
			_userCache = new UserCache(LDAPPathUtil.GetUniqueKey(path, user), Settings.UserCacheSize);
			_memberOfCache = new MemberOfCache(LDAPPathUtil.GetUniqueKey(path, user), Settings.MemberOfCacheSize);
			_membersCache = new MembersCache(LDAPPathUtil.GetUniqueKey(path, user), Settings.MembersCacheSize);
			if (useNotification)
			{
				_notificationProvider = DirectoryNotificationManager.GetProvider(path, user, password, attributeMapName);
				if (_notificationProvider != null)
				{
					_notificationProvider.ObjectCategoryAdded += OnObjectCategoryAdded;
					_notificationProvider.ObjectCategoryDeleted += OnObjectCategoryDeleted;
					_notificationProvider.ObjectCategoryModified += OnObjectCategoryModified;
					FullOuPath = LDAPPathUtil.GetFullOUName(path).ToLower();
				}
			}
		}

		public void ClearMemberOf()
		{
			_memberOfCache.Clear();
		}

		public void ClearMembers()
		{
			_membersCache.Clear();
		}

		public IEnumerable<string> GetMembers(string role)
		{
			Assert.ArgumentNotNullOrEmpty(role, "role");
			return _membersCache.GetMembers(role);
		}

		public IEnumerable<string> GetParentsRoles(string key)
		{
			Assert.ArgumentNotNullOrEmpty(key, "key");
			return _memberOfCache.GetParentRoles(key);
		}

		public MembershipUser GetUser(string name)
		{
			UserDefinition definition = _userCache.GetObject(name);
			if (definition != null)
			{
				return (SitecoreLdsMembershipUser)definition;
			}
			return null;
		}

		public bool? IsInRole(string name, string role)
		{
			Assert.ArgumentNotNullOrEmpty(role, "role");
			IEnumerable<string> parentRoles = _memberOfCache.GetParentRoles(name);
			if (parentRoles == null)
			{
				return null;
			}
			Func<string, bool> predicate = s => s == role;
			return parentRoles.FirstOrDefault(predicate) != null;
		}

		private void OnObjectCategoryAdded(object sender, ObjectCategoryAddedEventArgs e)
		{
		}

		private void OnObjectCategoryDeleted(object sender, ObjectCategoryDeletedEventArgs e)
		{
			_userCache.RemoveKeysContaining(e.UserName);
			_memberOfCache.RemoveKeysContaining(e.UserName);
			_membersCache.RemoveKeysContaining(e.UserName);
		}

		private void OnObjectCategoryModified(object sender, ObjectCategoryModifiedEventArgs e)
		{
			_userCache.RemoveKeysContaining(e.UserName);
			_memberOfCache.RemoveKeysContaining(e.UserName);
			_membersCache.RemoveKeysContaining(e.UserName);
		}

		public void RemoveMemberOf(string name)
		{
			_memberOfCache.RemoveKeysContaining(name);
		}

		public void RemoveRole(string name)
		{
			_membersCache.RemoveKeysContaining(name);
		}

		public void RemoveUser(string name)
		{
			_userCache.RemoveKeysContaining(name);
			_memberOfCache.RemoveKeysContaining(name);
		}

		public void SetMembers(string role, IEnumerable<string> members)
		{
			_membersCache.SetMembers(role, new UniqueList(members));
		}

		public void SetParentsRoles(string key, IEnumerable<string> roles)
		{
			_memberOfCache.SetParentRoles(key, new UniqueList(roles));
		}

		public void UpdateUser(string name, MembershipUser user)
		{
			_userCache.SetObject(name, (UserDefinition)user);
		}

		// Properties
		public string FullOuPath { get; private set; }

		internal DirectoryNotificationProvider NotificationProvider
		{
			get
			{
				return _notificationProvider;
			}
		}

		public bool UseNotification { get; set; }
	}

}
