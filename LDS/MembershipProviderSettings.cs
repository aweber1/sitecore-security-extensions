﻿using System;
using System.Collections.Specialized;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using LightLDAP;
using LightLDAP.ActiveDirectory;
using LightLDAP.Utility;
using Sitecore.SecurityExtensions.LDS.DataSource;
using System.DirectoryServices.Protocols;

namespace Sitecore.SecurityExtensions.LDS
{
	[StructLayout(LayoutKind.Sequential)]
	public struct MembershipProviderSettings
	{
		public string containerDn { get; set; }
		public object directoryInfo { get; set; }

		public void InitializeHidden(IAdMembershipProvider provider, bool isInnerFailed)
		{
			Type hiddenSettingsInType = provider.HiddenSettingsInType;
			directoryInfo = ReflectionUtil.GetField<object>(hiddenSettingsInType, "directoryInfo", provider);
			Type type = directoryInfo.GetType();
			if (isInnerFailed)
			{
				ReflectionUtil.SetField(hiddenSettingsInType, "initialized", provider, true);
				LdapConnection connection = ReflectionUtil.ExecuteMethod<LdapConnection>(type, "CreateNewLdapConnection", directoryInfo, new object[] { ReflectionUtil.GetField<AuthType>(hiddenSettingsInType, "authTypeForValidation", provider) });
				ReflectionUtil.SetField(hiddenSettingsInType, "connection", provider, connection);
			}
			MaxEmailLength = ReflectionUtil.GetField<int>(hiddenSettingsInType, "maxEmailLength", provider);
			MaxCommentLength = ReflectionUtil.GetField<int>(hiddenSettingsInType, "maxCommentLength", provider);
			MaxUsernameLength = ReflectionUtil.GetField<int>(hiddenSettingsInType, "maxUsernameLength", provider);
			UserNameIsUpn = ReflectionUtil.GetField<bool>(hiddenSettingsInType, "usernameIsUPN", provider);
			DirectoryType = ReflectionUtil.GetField<int>(type, "directoryType", directoryInfo);
			AdLockoutDuration = ReflectionUtil.GetField<TimeSpan>(type, "adLockoutDuration", directoryInfo);
			ClientSearchTimeout = ReflectionUtil.GetField<int>(type, "clientSearchTimeout", directoryInfo);
			ServerSearchTimeout = ReflectionUtil.GetField<int>(type, "serverSearchTimeout", directoryInfo);
			containerDn = ReflectionUtil.GetField<string>(type, "containerDN", directoryInfo);
			AuthType = ReflectionUtil.GetField<AuthenticationTypes>(type, "authenticationType", directoryInfo);
			AdsPath = ReflectionUtil.ExecuteMethod<string>(type, "GetADsPath", directoryInfo, new object[] { containerDn });
			UserName = ReflectionUtil.ExecuteMethod<string>(type, "GetUsername", directoryInfo, new object[0]);
			Password = ReflectionUtil.ExecuteMethod<string>(type, "GetPassword", directoryInfo, new object[0]);
			Root = new LightLDAP.ActiveDirectory.DirectoryEntry(AdsPath, UserName, Password, AuthType);
		}

		
		public void Initialize(NameValueCollection config)
		{
			AttributeMapUsername = config["attributeMapUsername"] ?? "userPrincipalName";
			AttributeMapEmail = config["attributeMapEmail"] ?? "mail";
			AttributeMapPasswordQuestion = config["attributeMapPasswordQuestion"];
			AttributeMapFailedPasswordAnswerLockoutTime = config["attributeMapFailedPasswordAnswerLockoutTime"];
			AttributeMapFailedPasswordAnswerCount = config["attributeMapFailedPasswordAnswerCount"];
			AttributeMapFailedPasswordAnswerTime = config["attributeMapFailedPasswordAnswerTime"];
			EnableSearchMethods = Utility.StringUtil.GetBool(config["enableSearchMethods"], false);
			PasswordAnswerAttemptLockoutDuration = Utility.StringUtil.GetIntValue(config["passwordAnswerAttemptLockoutDuration"], 30, false, 0);
			int result;
			int.TryParse(config["serverPageTimeLimit"], out result);
			ServerPageTimeLimit = result;
			ApplicationName = config["applicationName"];
			DefaultLastLockoutDate = new DateTime(0x6da, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			UseNotification = Utility.StringUtil.GetBool(config["useNotification"], true);
		}

		public bool EnableSearchMethods { get; private set; }
		public bool UseNotification { get; private set; }
		public DateTime DefaultLastLockoutDate { get; private set; }
		public string ApplicationName { get; private set; }
		public int ServerPageTimeLimit { get; private set; }
		public string AttributeMapFailedPasswordAnswerTime { get; private set; }
		public string AttributeMapFailedPasswordAnswerCount { get; private set; }
		public string AttributeMapFailedPasswordAnswerLockoutTime { get; private set; }
		public string AttributeMapPasswordQuestion { get; private set; }
		public string AttributeMapEmail { get; private set; }
		public string AttributeMapUsername { get; private set; }
		public int MaxEmailLength { get; private set; }
		public int MaxCommentLength { get; private set; }
		public int MaxUsernameLength { get; private set; }
		public int ServerSearchTimeout { get; private set; }
		public int ClientSearchTimeout { get; private set; }
		public TimeSpan AdLockoutDuration { get; private set; }
		public int DirectoryType { get; private set; }
		public IDirectoryEntry Root { get; private set; }
		public PartialDataSource Source { get; private set; }
		public bool UserNameIsUpn { get; private set; }
		public AuthenticationTypes AuthType { get; private set; }
		public string AdsPath { get; private set; }
		public string UserName { get; private set; }
		public string Password { get; private set; }
		public int PasswordAnswerAttemptLockoutDuration { get; private set; }
	}
}