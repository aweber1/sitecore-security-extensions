﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Diagnostics;

namespace Sitecore.SecurityExtensions.LDS.Resources
{
	public class SR
{
	// Fields
	private static readonly ResourceManager Rm = new ResourceManager("Sitecore.SecurityExtensions.Properties.Resources", Assembly.GetExecutingAssembly());

	internal static readonly string ACCESS_DENIED = "ACCESS_DENIED";
	internal static readonly string AD_CUSTOM_PROPERTY = "AD_CUSTOM_PROPERTY";
	internal static readonly string ADD_USER_TO_ROLE_NUMBER = "ADD_USER_TO_ROLE_NUMBER";
	internal static readonly string ADD_USERS_TO_ROLE_FATAL = "ADD_USERS_TO_ROLE_FATAL";
	internal static readonly string ADD_USERS_TO_ROLE_USER_ADD = "ADD_USERS_TO_ROLE_USER_ADD";
	internal static readonly string ADD_USERS_TO_ROLE_USERS_NUMBER = "ADD_USERS_TO_ROLE_USERS_NUMBER";
	internal static readonly string BLANK_NAME = "BLANK_NAME";
	internal static readonly string CAN_NOT_DELETE_ROLE = "CAN_NOT_DELETE_ROLE";
	internal static readonly string COM_EXCEPTION = "COM_EXCEPTION";
	internal static readonly string CREATE_ROLE_START = "CREATE_ROLE_START";
	internal static readonly string CREATE_USER_DONE = "CREATE_USER_DONE";
	internal static readonly string CREATE_USER_FAILED = "CREATE_USER_FAILED";
	internal static readonly string CREATE_USER_START = "CREATE_USER_START";
	internal static readonly string DELETE_INACTIVE_PROFILES = "DELETE_INACTIVE_PROFILES";
	internal static readonly string DELETE_PROFILE_PROPERTY = "DELETE_PROFILE_PROPERTY";
	internal static readonly string DELETE_PROFILE_START = "DELETE_PROFILE_START";
	internal static readonly string DELETE_PROFILES_END = "DELETE_PROFILES_END";
	internal static readonly string DELETE_PROFILES_START = "DELETE_PROFILES_START";
	internal static readonly string DELETE_ROLE_START = "DELETE_ROLE_START";
	internal static readonly string DELETED_INACTIVE_NUMBER = "DELETED_INACTIVE_NUMBER";
	internal static readonly string DUPLICATED_EMAIL = "DUPLICATED_EMAIL";
	internal static readonly string EMPTY_CONNECTION_STRING = "EMPTY_CONNECTION_STRING";
	internal static readonly string FIND_INACTIVE_BY_NAME_END = "FIND_INACTIVE_BY_NAME_END";
	internal static readonly string FIND_INACTIVE_BY_NAME_NUMBER = "FIND_INACTIVE_BY_NAME_NUMBER";
	internal static readonly string FIND_INACTIVE_BY_NAME_START = "FIND_INACTIVE_BY_NAME_START";
	internal static readonly string FIND_PROFILE_BY_NAME = "FIND_PROFILE_BY_NAME";
	internal static readonly string FIND_PROFILE_BY_NAME_FOUND = "FIND_PROFILE_BY_NAME_FOUND";
	internal static readonly string FIND_PROFILE_BY_NAME_NUMBER = "FIND_PROFILE_BY_NAME_NUMBER";
	internal static readonly string FIND_USERS_BY_EMAIL_END = "FIND_USERS_BY_EMAIL_END";
	internal static readonly string FIND_USERS_BY_EMAIL_START = "FIND_USERS_BY_EMAIL_START";
	internal static readonly string FIND_USERS_BY_NAME_END = "FIND_USERS_BY_NAME_END";
	internal static readonly string FIND_USERS_BY_NAME_START = "FIND_USERS_BY_NAME_START";
	internal static readonly string GET_ALL_INACTIVE_PROFILE_FOUND = "GET_ALL_INACTIVE_PROFILE_FOUND";
	internal static readonly string GET_ALL_INACTIVE_PROFILE_NUMBER = "GET_ALL_INACTIVE_PROFILE_NUMBER";
	internal static readonly string GET_ALL_INACTIVE_PROFILE_START = "GET_ALL_INACTIVE_PROFILE_START";
	internal static readonly string GET_ALL_PROFILES_FOUND = "GET_ALL_PROFILES_FOUND";
	internal static readonly string GET_ALL_PROFILES_NUMBER = "GET_ALL_PROFILES_NUMBER";
	internal static readonly string GET_ALL_PROFILES_START = "GET_ALL_PROFILES_START";
	internal static readonly string GET_ALL_USERS_END = "GET_ALL_USERS_END";
	internal static readonly string GET_ALL_USERS_START = "GET_ALL_USERS_START";
	internal static readonly string GET_PROPERTY_VALUES_PROPERTY = "GET_PROPERTY_VALUES_PROPERTY";
	internal static readonly string GET_PROPERTY_VALUES_USER = "GET_PROPERTY_VALUES_USER";
	internal static readonly string GET_ROLE_MEMBERS = "GET_ROLE_MEMBERS";
	internal static readonly string GET_ROLE_MEMBERS_CONTAINS = "GET_ROLE_MEMBERS_CONTAINS";
	internal static readonly string GET_ROLE_MEMBERS_SEARCH = "GET_ROLE_MEMBERS_SEARCH";
	internal static readonly string GET_USER_FOUND = "GET_USER_FOUND";
	internal static readonly string GET_USER_MEMBERSHIP_INDIRECT = "GET_USER_MEMBERSHIP_INDIRECT";
	internal static readonly string GET_USER_MEMBERSHIP_NESTED = "GET_USER_MEMBERSHIP_NESTED";
	internal static readonly string GET_USER_MEMBERSHIP_PRIMARY = "GET_USER_MEMBERSHIP_PRIMARY";
	internal static readonly string GET_USER_NOT_FOUND = "GET_USER_NOT_FOUND";
	internal static readonly string GET_USER_PK_FOUND = "GET_USER_PK_FOUND";
	internal static readonly string GET_USER_PK_NOT_FOUND = "GET_USER_PK_NOT_FOUND";
	internal static readonly string GET_USER_PK_START = "GET_USER_PK_START";
	internal static readonly string GET_USER_START = "GET_USER_START";
	internal static readonly string INVALIDE_CONNECTION_STRING = "INVALIDE_CONNECTION_STRING";
	internal static readonly string MISSING_CONNECTION_STRING = "MISSING_CONNECTION_STRING";
	internal static readonly string NOT_AD_CUSTOM_PROPERTY = "NOT_AD_CUSTOM_PROPERTY";
	internal static readonly string NOT_INITIALIZED = "NOT_INITIALIZED";
	internal static readonly string PAGE_INDEX_INVALID = "PAGE_INDEX_INVALID";
	internal static readonly string PAGE_INDEX_NEGATIVE = "PAGE_INDEX_NEGATIVE";
	internal static readonly string PAGE_INVALID = "PAGE_INVALID";
	internal static readonly string PAGE_SIZE_INVALID = "PAGE_SIZE_INVALID";
	internal static readonly string PAGE_SIZE_LESS_THAN_1 = "PAGE_SIZE_LESS_THAN_1";
	internal static readonly string PAGE_UPPER_LIMIT = "PAGE_UPPER_LIMIT";
	internal static readonly string PARAM_CANT_BE_EMPTY = "PARAM_CANT_BE_EMPTY";
	internal static readonly string PARAM_IS_TOO_LONG = "PARAM_IS_TOO_LONG";
	internal static readonly string READ_ONLY_MODE_ENABLED = "READ_ONLY_MODE_ENABLED";
	internal static readonly string REMOVE_FROM_ROLE = "REMOVE_FROM_ROLE";
	internal static readonly string REMOVE_USER_FROM_ROLE_START = "REMOVE_USER_FROM_ROLE_START";
	internal static readonly string REMOVE_USERS_FROM_ROLE_FATAL = "REMOVE_USERS_FROM_ROLE_FATAL";
	internal static readonly string REMOVE_USERS_FROM_ROLE_FOUND = "REMOVE_USERS_FROM_ROLE_FOUND";
	internal static readonly string REMOVE_USERS_FROM_ROLE_START = "REMOVE_USERS_FROM_ROLE_START";
	internal static readonly string ROLE_ALREADY_EXIST = "ROLE_ALREADY_EXIST";
	internal static readonly string ROLE_CANT_BE_BLANK = "ROLE_CANT_BE_BLANK";
	internal static readonly string ROLE_CREATE_FAILED = "ROLE_CREATE_FAILED";
	internal static readonly string ROLE_CREATED = "ROLE_CREATED";
	internal static readonly string ROLE_DELETE_FATAL = "ROLE_DELETE_FATAL";
	internal static readonly string ROLE_DELETED = "ROLE_DELETED";
	internal static readonly string ROLE_EXIST = "ROLE_EXIST";
	internal static readonly string ROLE_FOUND = "ROLE_FOUND";
	internal static readonly string ROLE_IS_NOT_EMPTY = "ROLE_IS_NOT_EMPTY";
	internal static readonly string ROLE_NOT_DELETED = "ROLE_NOT_DELETED";
	internal static readonly string ROLE_NOT_EXIST = "ROLE_NOT_EXIST";
	internal static readonly string ROLE_OR_USER_LIST_IS_EMPTY = "ROLE_OR_USER_LIST_IS_EMPTY";
	internal static readonly string ROLE_OR_USERS_NOT_FOUND = "ROLE_OR_USERS_NOT_FOUND";
	internal static readonly string ROLE_PROVIDER_SET_NAME = "ROLE_PROVIDER_SET_NAME";
	internal static readonly string ROLES_FOUND = "ROLES_FOUND";
	internal static readonly string SEARCH_NOT_SUPPORTED = "SEARCH_NOT_SUPPORTED";
	internal static readonly string SET_PROPERTY_VALUES_PROPERTY = "SET_PROPERTY_VALUES_PROPERTY";
	internal static readonly string THERE_IS_A_NAMIG_VIOLATION = "THERE_IS_A_NAMIG_VIOLATION";
	internal static readonly string UNKNOWN_ERROR = "UNKNOWN_ERROR";
	internal static readonly string UPDATE_USER_COMMENT = "UPDATE_USER_COMMENT";
	internal static readonly string UPDATE_USER_DONE = "UPDATE_USER_DONE";
	internal static readonly string UPDATE_USER_START = "UPDATE_USER_START";
	internal static readonly string USER_CANT_BE_BLANK = "USER_CANT_BE_BLANK";
	internal static readonly string USER_CENT_BE_ROMOVED_FROM_ROLE = "USER_CENT_BE_ROMOVED_FROM_ROLE";
	internal static readonly string USER_CONTAINS_BACKSLASH = "USER_CONTAINS_BACKSLASH";
	internal static readonly string USER_IN_ROLE_INDIRECT = "USER_IN_ROLE_INDIRECT";
	internal static readonly string USER_IN_ROLE_NOT_FOUND = "USER_IN_ROLE_NOT_FOUND";
	internal static readonly string USER_IN_ROLE_USER_NOT_FOUND = "USER_IN_ROLE_USER_NOT_FOUND";
	internal static readonly string USER_MEMBEROF_FOUND = "USER_MEMBEROF_FOUND";
	internal static readonly string USER_NOT_FOUND = "USER_NOT_FOUND";
	internal static readonly string USER_NOT_IN_ROLE = "USER_NOT_IN_ROLE";
	internal static readonly string USER_NOT_IN_ROLE_FRENDLY = "USER_NOT_IN_ROLE_FRENDLY";
	internal static readonly string USER_PRIMARY_GROUP_FOUND = "USER_PRIMARY_GROUP_FOUND";
	internal static readonly string USER_PRIMARY_GROUP_FOUND_USER = "USER_PRIMARY_GROUP_FOUND_USER";
	internal static readonly string USER_PRIMARY_GROUP_START = "USER_PRIMARY_GROUP_START";
	internal static readonly string USERS_TO_ROLE_UNEXISTED = "USERS_TO_ROLE_UNEXISTED";

	// Methods
	public static string GetString(string name)
	{
		Assert.IsNotNullOrEmpty(name, "name");
		return Rm.GetString(name);
	}

	public static string GetString(string name, string param)
	{
		Assert.IsNotNullOrEmpty(name, "name");
		return string.Format(Rm.GetString(name), param);
	}

	public static string GetString(string name, params object[] parameters)
	{
		Assert.IsNotNullOrEmpty(name, "name");
		return string.Format(Rm.GetString(name), parameters);
	}
}
 

}
