﻿This code is dependent on the Sitecore Active Directory module. Follow the installation instructions provided with that module.
[http://sdn.sitecore.net/Products/AD/AD11.aspx](http://sdn.sitecore.net/Products/AD/AD11.aspx)
	
For more information, see this blog post:
[http://www.sitecore.net/Community/Technical-Blogs/SitecoreBetter/Posts/2013/10/Sitecore-and-Microsoft-Active-Directory-Lightweight-Directory-Services-AD-LDS.aspx](http://www.sitecore.net/Community/Technical-Blogs/SitecoreBetter/Posts/2013/10/Sitecore-and-Microsoft-Active-Directory-Lightweight-Directory-Services-AD-LDS.aspx)
